import React, { useEffect, useState } from "react";

// newPeople
const numOfSecForPeopleToCome = 10;

export default function NewPeople() {
  const [timeLeft, setTimeLeft] = useState(numOfSecForPeopleToCome);
  const [people, setPeople] = useState(0);

  useEffect(() => {
    // Create an interval that fires the addResource() function every 10 seconds
    // and updates the timeLeft state variable
    const interval = setInterval(() => {
      if (timeLeft == 0) {
        setTimeLeft(numOfSecForPeopleToCome);
        // @ts-ignore
        window.addResource("people", 1, true);
        setPeople(people + 1);
      } else {
        setTimeLeft(timeLeft - 1);
      }
    }, 1000);
    // Clean up the interval when the component unmounts
    return () => clearInterval(interval);
  }, [timeLeft]);
  // The component content here
  return (
    <>
      Planet population (👶) : {people}

      {/* Display the timeLeft state variable as the countdown timer */}
      <p> Next 👶 in {timeLeft} </p>
    </>
  );
}
