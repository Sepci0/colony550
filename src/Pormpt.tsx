import React, { useEffect, useState } from "react";
import styled from "styled-components";

const Container = styled.div`
  position: fixed;
  top: 50px;
  margin-left: 10px;
  /* left: 20; */
  /* right: 0; */

  border-radius: 10px;
  padding: 16px;
  background-color: #fff;
  border-top: 1px solid #ccc;
  /* display: flex; */
  align-items: center;
  justify-content: center;
`;

const HiddenContainer = styled(Container)`
  &.hidden {
    visibility: hidden;
  }
`;

const Text = styled.div`
  font-size: 16px;
  color: #333;
`;
const Prompt = () => {
  const [text, setText] = useState("");

  // @ts-ignore
  window.showPrompt = (promptText: string) => {
    setText(promptText);
  };

  const handleFinish = () => {
    setText("");
  };
  useEffect(() => {
    const timer = setTimeout(() => {
      handleFinish();
    }, 3000);

    return () => {
      clearTimeout(timer);
    };
  }, [text]);

  return (
    <HiddenContainer className={text ? "" : "hidden"}>
      <Text>{text}</Text>
      <div>
        <button type='button' onClick={handleFinish}>
          Close
        </button>
      </div>
    </HiddenContainer>
  );
};

export default Prompt;
