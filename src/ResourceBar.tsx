// @ts-nocheck

import React from "react";
import styled from "styled-components";
import { getColor } from "./colors";

const barHeight = 200;

const BackgroundDiv = styled.div`
  background-color: #a7a7a7;
  height: ${barHeight}px;
  width: 50px;
  border-radius: 10px;
  margin: 10px;
`;

const InsideBar = styled.div`
  position: absolute;
  /* @ts-ignore */
  height: ${(p) => Math.floor((p.value * barHeight) / 100)}px;
  /* @ts-ignore */
  margin-top: ${(p) => Math.floor(((100 - p.value) * barHeight) / 100)}px;
  width: 50px;
  border-radius: 10px;
  /* @ts-ignore */
  background-color: ${(p) => getColor(p.type)};
`;

let maxValue = 213.7;

export default function ResourceBar({ value, type }: any) {
  console.log(value, type, Math.floor((value / maxValue) * 100));
  return (
    <BackgroundDiv>
      {/* @ts-ignore */}
      <InsideBar value={Math.floor((value / maxValue) * 100)} type={type} />
    </BackgroundDiv>
  );
}
