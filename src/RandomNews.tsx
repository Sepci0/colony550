import React, { useEffect, useState } from "react";

const NEWS_ITEMS = [
  "Scientists discover new form of life on Mars",
  "Mars colony Copernicus550 reaches population milestone",
  "Mars colonists successfully grow first crops",
  "New solar power plant brings energy independence to Mars colony",
  "Mars colony establishes first government and elected leader",
  "Mars colony successfully mines and processes first iron ore",
  "Mars colony discovers and begins to tap underground water source",
  "Mars colonists make contact with neighboring colony, Olympus Mons",
  "Mars colony Copernicus550 celebrates first anniversary",
  "Mars colonists complete construction of first permanent housing structures",
  "Mars colony opens first school for children",
  "Mars colony establishes first trade agreement with Earth",
  "Mars colony Copernicus550 receives first supply ship from Earth",
  "Mars colonists develop new techniques for extracting oxygen from Mars atmosphere",
  "Mars colony establishes first public park and recreational area",
  "Mars colonists make important discoveries in Mars geology and history",
  "Mars colony Copernicus550 becomes self-sustaining in food production",
  "Mars colonists develop new methods for long-distance communication",
  "Mars colony Copernicus550 receives first visitors from Earth",
];

const RandomNews: React.FC = () => {
  const [currentHeadline, setCurrentHeadline] = useState("Mars colony Copernicus550 established");
  const [shownNewsItems, setShownNewsItems] = useState([]);

  useEffect(() => {
    const interval = setInterval(() => {
      const availableNewsItems = NEWS_ITEMS.filter(
        //@ts-ignore
        (item) => !shownNewsItems.includes(item)
      );
      if (availableNewsItems.length === 0) {
        setShownNewsItems([]);
        return;
      }

      const randomIndex = Math.floor(Math.random() * availableNewsItems.length);
      const headline = availableNewsItems[randomIndex];
      setCurrentHeadline(headline);
      //@ts-ignore
      setShownNewsItems([...shownNewsItems, headline]);
    }, 10000);

    return () => clearInterval(interval);
  }, [shownNewsItems]);

  return <h3>{currentHeadline}</h3>;
};

export default RandomNews;
