import { useEffect, useRef, useState } from "react";
import { unstable_HistoryRouter, useNavigate } from "react-router-dom";
import styled from "styled-components";
import ResourceBar from "./ResourceBar";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const Title = styled.h1`
  font-size: 24px;
  font-weight: bold;
  /* color: #333; */
`;

const startPeople = 0;

// water drinking
const drinkingPerSecond = 3.3;

const Resources = () => {
  // Starting resources
  const [resourcesObject, setResourceObject] = useState({
    water: 100,
    energy: 150,
    iron: 100,
    people: startPeople,
  });
  const [maxPeople, setMaxPeople] = useState(startPeople);

  const intervalRef = useRef();
let navigate = useNavigate();


  useEffect(() => {
    // @ts-ignore
    intervalRef.current = setInterval(() => {
      setResourceObject((old) => ({
        ...old,
        water: old.water - drinkingPerSecond * maxPeople,
      }));

      // Check if resources are lower than -100 and show prompt
      if (resourcesObject.water <= -100 || resourcesObject.energy <= -100) {
        // @ts-ignore
        window.showPrompt("you lose!");
        navigate("/lost")

      }
    }, 1000);

    // Clean up the interval when the component unmounts
    return () => clearInterval(intervalRef.current);
  }, [maxPeople]);

  const increment = (resource: string) => {
    let workingFee = -20;
    if (resource == "energy") {
      workingFee = 0;
    }

    let income = 10;

    if (resource == "iron") {
      income = 5;
    }

    setResourceObject((old) => ({
      ...old,
      // @ts-ignore
      [resource]: (old[resource] || 0) + income,
      energy: (old["energy"] || 0) + workingFee + 10,
    }));
  };

  const addResource = (type: string, number: number, isNewPeople?: boolean) => {
    if (isNewPeople) {
      setMaxPeople((old) => old + 1);
    }
    // Use the functional form of setResourceObject to update the state
    setResourceObject((prevResourcesObject) => ({
      ...prevResourcesObject,
      //@ts-ignore
      [type]: prevResourcesObject[type] + number,
    }));
  };

  //@ts-ignore
  window.addResource = addResource;

  //@ts-ignore
  window.getResource = (type) => {
    // @ts-ignore
    return resourcesObject[type] || 0;
  };

  // @ts-ignore
  window.increment = increment;

  return (
    <Container>
      <div style={{ display: "flex" }}>
        <div>
          <ResourceBar
            type={"water"}
            value={Math.floor(resourcesObject.water)}
          />
          Water
        </div>
        <div>
          <ResourceBar type={"energy"} value={resourcesObject.energy} />
          Energy
        </div>
      </div>

      <Title>
        {/* @ts-ignore */}
        🔧: {Math.floor(resourcesObject.iron) || "0"} <br />
      </Title>
      <Title>
        {/* @ts-ignore */}
        👶: {Math.floor(resourcesObject.people) || "0"} <br />
      </Title>
    </Container>
  );
};

export default Resources;
