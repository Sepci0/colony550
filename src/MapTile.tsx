import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { getColor, getPrice } from "./colors";

// tileSize
export const TileWidth = 60;

const MapTile = ({ tileId }: any) => {
  const [currentBuilding, setState] = useState({
    name: tileId == "1,9" ? "iron" : "idle",
    inhabited: false,
  });

  const handleClick = () => {
    if (currentBuilding.name !== "idle") {
      if (!currentBuilding.inhabited) {
        // @ts-ignore
        if (window.getResource("people") > 0) {
          // @ts-ignore
          window.addResource("people", -1);
          setState({
            ...currentBuilding,
            inhabited: !currentBuilding.inhabited,
          });
        } else {
          // @ts-ignore
          window.showPrompt("Do obsadzenia budynku potrzeba ludzi!");
        }
      } else {
        // @ts-ignore
        window.addResource("people", +1);
        clearInterval(interval.current);
        setState({
          ...currentBuilding,
          inhabited: !currentBuilding.inhabited,
        });
      }
    } else {
      const currentSelected = localStorage.getItem("currentBuilding");
      // @ts-ignore
      if (window.getAvailableBuilding(currentSelected) > 0) {
        // @ts-ignore
        if (window.getResource("iron") >= getPrice(currentSelected)) {
          setState({ name: currentSelected || "idle", inhabited: false });
          // @ts-ignore
          window.addResource("iron", -getPrice(currentSelected));
          // @ts-ignore
          window.addAvailableBuilding(currentSelected, -1);
        } else {
          // @ts-ignore
          window.showPrompt("Nie masz żelaza!");
        }
      } else {
        if (currentSelected == "idle") {
          // @ts-ignore
          window.showPrompt("Wybierz kopalnie jaką chcesz zbudować");
        } else {
          // @ts-ignore
          window.showPrompt("Brakuje kopalni! Zamów kopalnie z ziemi.");
        }
      }
    }
  };

  const interval = useRef(0);

  useEffect(() => {
    if (currentBuilding.inhabited) {
      if (currentBuilding.name !== "idle") {
        interval.current = setInterval(() => {
          if (currentBuilding.inhabited) {
            // @ts-ignore
            window.increment(currentBuilding.name);
          }
        }, 1000);
      } else {
        clearInterval(interval.current);
      }
    }
  }, [currentBuilding.name, currentBuilding.inhabited]);

  return (
    <Tile building={currentBuilding.name} onClick={handleClick}>
      {currentBuilding.name === "idle" ? null : (
        <>
          {currentBuilding.inhabited ? null : (
            <img src='/img/off_indicator.png' width={TileWidth - 20} />
          )}
        </>
      )}
    </Tile>
  );
};
export default MapTile;

const Tile = styled.div<{ building: string }>`
  cursor: pointer;
  user-select: none;
  width: ${TileWidth}px;
  height: ${TileWidth}px;
  /* background-color: ${(props) => getColor(props.building)}; */
  background-image: url("/img/${(props) => props.building}-building.png");
  background-size: ${TileWidth - 20}px;
  background-position: center;
  background-repeat: no-repeat;
  border: 1px solid #333;
  display: flex;
  align-items: center;
  justify-content: center;
`;
