import React, { useState } from "react";
import styled from "styled-components";
import { getColor } from "./colors";

export const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50px;
  height: 50px;
  border: none;
  background: transparent;
  cursor: pointer;
  outline: none;


  border: solid 1px white;

  padding: 30px;
  ${(p) =>
    // @ts-ignore
    // background: radial-gradient(71.95% 73.17% at 50% 50%, rgba(0, 0, 0, 0) 36.98%, #ff66600 100%);
    p.selected
      ? `
      background-color: #666;
      `
      : ""}

  /* padding: 20px; */
      > img {
    margin: 10px;

    ${(p) =>
      // @ts-ignore
      // background: radial-gradient(71.95% 73.17% at 50% 50%, rgba(0, 0, 0, 0) 36.98%, #ff66600 100%);
      p.disabled
        ? `
      opacity: 0.5;
      `
        : ""}
  }

  :hover {
    box-shadow: 0px 0px 10px 2px rgba(255, 255, 255, 0.5);
  }
`;

const Container = styled.div`
  font-family: "Quantico";
  font-style: italic;
  font-weight: 400;
  font-size: 23px;
  line-height: 33px;

  display: flex;
  /* flex-direction: column; */
  align-items: center;

  > div {
    display: flex;
    margin:10px;
    flex-direction: column;
    > span {
      padding: 10px;
    }
  }
`;

const BuildingMenu = () => {
  const [selected, setSelected] = useState("");

  const handleClick = (building: string) => {
    setSelected(building);
    localStorage.setItem("currentBuilding", building);
  };

  const [numOfAvailableBuildings, setNumOfAvailableBuildings] = useState({
    water: 3,
    energy: 3,
    iron: 3,
    // people: 3,
  });

  const addAvailableBuilding = (type: string, number: number) => {
    setNumOfAvailableBuildings({
      ...numOfAvailableBuildings,
      //@ts-ignore
      [type]: numOfAvailableBuildings[type] + number,
    });
  };

  //@ts-ignore
  window.addAvailableBuilding = addAvailableBuilding;

  //@ts-ignore
  window.getAvailableBuilding = (type) => {
    // @ts-ignore
    return numOfAvailableBuildings[type] || 0;
  };

  return (
    <>
      <p>
        Building cost 40 iron.
        <br />
        <br />
        Select type, and press on the map to build
      </p>
      <Container>
        {Object.entries(numOfAvailableBuildings).map(([type, num]) => (
          <div key={type}>
            <Button
              // @ts-ignore
              selected={selected == type}
              onClick={() => handleClick(type)}>
              {/* {type} */}
              <img src={`/img/${type}-building.png`} alt={type} />
            </Button>
            <span>{num}</span>
          </div>
        ))}
      </Container>
    </>
  );
};

export default BuildingMenu;
