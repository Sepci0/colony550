import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

export const MenuContainer = styled.div`
  > a {
    color: white;
    text-transform: uppercase;
    display: block;
    border: solid 1px white;
    padding: 10px;
    border-radius: 10px;
    margin: 20px;
  }
`;

export default function Credits() {
  return (
    <MenuContainer>
      <h1>Credits</h1>
      <p>
        Code by{" "}
        <a target={"_blank"} href='https://sepci0.pl'>
          Sepci0 (link)
        </a>
      </p>
      <br />
      <p>
        UX/UI by{" "}
        <a
          target={"_blank"}
          href='https://www.linkedin.com/in/jakub-galanciak-kugalus/'>
          Kugalus (link)
        </a>
      </p>
      <p>
        With a lot of help from chatGPT
        <a target={"_blank"} href='https://sharegpt.com/c/cCOWA5p'>
          ChatGPT
        </a>
      </p>
      <br></br>
      <Link to='/'>Back to menu</Link>
    </MenuContainer>
  );
}
