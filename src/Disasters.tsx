import { useEffect, useState } from "react";
import styled from "styled-components";

const DisasterButton = styled.button`
  font-size: 16px;
  font-weight: bold;
  color: #333;
  padding: 10px 20px;
  border: none;
  cursor: pointer;
`;

const disasters: Disaster[] = [
  {
    name: "fire",
    modifiers: [
      {
        resourceName: "water",
        modifierValue: -5,
      },
    ],
  },
  {
    name: "flood",
    modifiers: [
      {
        resourceName: "energy",
        modifierValue: -3,
      },
    ],
  },
  {
    name: "earthquake",
    modifiers: [
      {
        resourceName: "iron",
        modifierValue: -5,
      },
    ],
  },
  {
    name: "storm",
    modifiers: [
      {
        resourceName: "people",
        modifierValue: -2,
      },
    ],
  },
];

interface Disaster {
  name: string;
  modifiers: {
    resourceName: string;
    modifierValue: number;
  }[];
}

const Disasters = () => {
  const [activeDisasters, setActiveDisasters] = useState<Disaster[]>([]);

  // Use a Map object to store the current modifier values
  const [modifiers, setModifiers] = useState(
    new Map([
      ["water", 0],
      ["energy", 0],
      ["iron", 0],
      ["people", 0],
    ])
  );

  // Iterate over the active disasters and update the modifiers Map
  useEffect(() => {
    // Reset the modifiers Map to all zeros
    // setModifiers(new Map());
    let newMap = new Map();
    // Iterate over the active disasters and update the modifiers Map
    activeDisasters.forEach((disaster) => {
      disaster.modifiers.forEach((modifier) => {
        // Use Map.set() to update the value for the specified key
        newMap.set(modifier.resourceName, modifier.modifierValue);
      });
    });
    setModifiers(newMap);
  }, [activeDisasters]);

  // Apply the modifiers every second
  useEffect(() => {
    const interval = setInterval(() => {
      // Use Map.forEach() to iterate over the entries in the Map
      modifiers.forEach((value, key) => {
        if (value !== 0) {
          // @ts-ignore
          window.addResource(key, value);
        }
      });
    }, 1000);

    return () => clearInterval(interval);
  }, [modifiers]);

  const handleClick = (disaster: Disaster) => {
    if (activeDisasters.includes(disaster)) {
      // Remove the disaster from the activeDisasters array
      setActiveDisasters(activeDisasters.filter((d) => d !== disaster));
    } else {
      // Add the disaster to the activeDisasters array
      setActiveDisasters([...activeDisasters, disaster]);
    }
  };

  return (
    <div>
      <div>
        {disasters.map((disaster) => (
          <DisasterButton
            key={disaster.name}
            onClick={() => handleClick(disaster)}>
            {disaster.name}
          </DisasterButton>
        ))}
      </div>
    </div>
  );
};

export default Disasters;
