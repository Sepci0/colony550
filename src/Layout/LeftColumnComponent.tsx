import React from "react";
import styled from "styled-components";
import Disasters from "../Disasters";
import NewPeople from "../NewPeople";
import RandomNews from "../RandomNews";
import Resources from "../ResoucreBar";

const Frame17 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 30px;
  gap: 40px;
  background: #1e1e1e;
`;

const Group17 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px;
  gap: 10px;

  flex: none;
  order: 0;
  align-self: stretch;
  flex-grow: 1;
`;

const ResourcesTitle = styled.div`
  font-family: "Quantico";
  font-style: normal;
  font-weight: 400;
  font-size: 32px;
  line-height: 46px;
  text-align: center;

  color: #ffffff;
`;

const WhiteBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 15px;
  gap: 25px;

  background: #171717;
  border: 0.5px solid #ffffff;
  border-radius: 10px;
`;
const MetersInnerContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0px;
  gap: 20px;
`;

const IronNumContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 10px;
`;

const PeopleContainer = styled.div`
  margin-top: 30px;
`;

export default function LeftColumnComponent() {
  return (
    <Frame17>
      <Group17>
        <ResourcesTitle>RESOURCES</ResourcesTitle>
        <WhiteBox>
          <MetersInnerContainer>
            {/* <div>22</div>
            <div>24</div> */}
            <Resources />
          </MetersInnerContainer>
        </WhiteBox>
        <PeopleContainer>
          <NewPeople />
          {/* Total 🧒:1000 <br />
          Available 🧒: 8 */}
        </PeopleContainer>
        <WhiteBox>
          <RandomNews />
        </WhiteBox>
      </Group17>
      {/* <Group17>
        <ResourcesTitle>Disasters</ResourcesTitle>
        <WhiteBox>
          <Disasters/>
          ❄ Blizzard
          <p>It makes your buildings use 2x Energy</p>
        </WhiteBox>
      </Group17> */}

      {/* LeftColumnComponent */}
    </Frame17>
  );
}
