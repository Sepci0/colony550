import React from "react";
import styled from "styled-components";
import BuildingMenu from "../BuildingMenu";
import RocketManager from "../RocketManager";

const Frame17 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 30px;
  gap: 40px;
  background: #1e1e1e;
`;

const Group17 = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0px;
  gap: 10px;

  flex: none;
  order: 0;
  align-self: stretch;
  flex-grow: 1;
`;

const ResourcesTitle = styled.div`
  font-family: "Quantico";
  font-style: normal;
  font-weight: 400;
  font-size: 32px;
  line-height: 46px;
  text-align: center;

  color: #ffffff;
`;

const WhiteBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 15px;
  gap: 25px;

  background: #171717;
  border: 0.5px solid #ffffff;
  border-radius: 10px;
`;
const MetersInnerContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0px;
  gap: 20px;
`;

const IronNumContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 10px;
`;

const PeopleContainer = styled.div`
  margin-top: 30px;
`;

export default function RightColumnComponent() {
  return (
    <Frame17>
      <Group17>
        <ResourcesTitle>Buildings</ResourcesTitle>
        <WhiteBox>
          <Group17>
            <BuildingMenu />
          </Group17>
        </WhiteBox>
      </Group17>
      <Group17>
        <ResourcesTitle>Rockets</ResourcesTitle>
        <WhiteBox>
          <RocketManager/>
        </WhiteBox>
      </Group17>

      {/* LeftColumnComponent */}
    </Frame17>
  );
}
