import { useState } from "react";
import reactLogo from "./assets/react.svg";
import MapGenerator from "./MapGenerator";
import { BrowserRouter, Route, Link, Routes } from "react-router-dom";
import BuildingMenu from "./BuildingMenu";
import Resources from "./ResoucreBar";
import Prompt from "./Pormpt";
import RocketManager from "./RocketManager";
import Disasters from "./Disasters";
import Map from "./Map";
import NewPeople from "./NewPeople";
import styled from "styled-components";
import LeftColumnComponent from "./Layout/LeftColumnComponent";
import RightColumnComponent from "./Layout/RightColumnComponent";
import MainMenu from "./MainMenu";
import Credits from "./Credits";
import Lost from "./Lost";

const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
`;

const StyledLeftColumn = styled.div`
  flex: 1;
  width: 1/8;
  height: 100%;
`;

const StyledMiddleColumn = styled.div`
  flex: 6;
  width: 6/8;
  height: 100%;
`;

const StyledRightColumn = styled.div`
  flex: 1;
  width: 1/8;
  height: 100%;
`;

// const ThreeColumnLayout: React.FC = () => {
//   return (
//     <StyledContainer>
//       <StyledLeftColumn />
//       <StyledMiddleColumn />
//       <StyledRightColumn />
//     </StyledContainer>
//   );
// };

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<MainMenu />} />
          <Route path='/credits' element={<Credits />} />
          <Route path='/lost' element={<Lost />} />
          <Route
            path='/game'
            element={
              <>
                <StyledContainer>
                  <StyledLeftColumn>
                    <LeftColumnComponent />
                  </StyledLeftColumn>
                  <StyledMiddleColumn>
                    <MapGenerator />
                  </StyledMiddleColumn>
                  <StyledRightColumn>
                    <RightColumnComponent />
                  </StyledRightColumn>
                </StyledContainer>
              </>
            }
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
