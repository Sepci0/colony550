import { useEffect, useState } from "react";
import styled from "styled-components";
import { Button } from "./BuildingMenu";
import { getColor } from "./colors";

let timerSize = 2;

const ContainerRocket = styled.div`
  font-family: "Quantico";
  font-style: italic;
  font-weight: 400;
  font-size: 23px;
  line-height: 33px;

  display: flex;
  align-items: center;

  > div {
    display: flex;
    flex-direction: column;
    margin: 10px;
    > span {
      padding: 10px;
    }
  }
`;

const RocketManager = () => {
  const [launched, setLaunched] = useState(false);
  const [timer, setTimer] = useState(timerSize);
  const [selectedType, setSelectedType] = useState<string | null>(null);

  const launchTimer = () => {
    setLaunched(true);
    setTimer(timerSize);
  };

  useEffect(() => {
    if (launched) {
      const interval = setInterval(() => {
        if (timer > 0) {
          setTimer(timer - 1);
        }
      }, 1000);

      return () => clearInterval(interval);
    }
  }, [launched, timer]);

  // Add the selected resource when the timer reaches 0
  useEffect(() => {
    if (timer === 0 && selectedType) {
      console.log("fire +1");
      if (selectedType == "people") {
        // @ts-ignore
        window.addResource(selectedType, +1, true);
      } else {
        // @ts-ignore
        window.addAvailableBuilding(selectedType, +1);
      }
      setSelectedType(null);
      setLaunched(false);
    }
  }, [selectedType, timer]);

  const handleButtonClick = (type: string) => {
    setSelectedType(type);
    launchTimer();
  };

  return (
    <>
      <div>
        Deliver new building
        <br/>
        {timer ? <p>Received in : {timer}s</p> : <p>Deliver time: 2s</p>}
      </div>
      <ContainerRocket>
        <div>
          <Button
            disabled={launched}
            onClick={() => handleButtonClick("water")}>
            <img src={`/img/water-rocket.svg`} />
          </Button>
          <p> +1 water</p>
        </div>
        <div>
          <Button
            disabled={launched}
            onClick={() => handleButtonClick("energy")}>
            <img src={`/img/energy-rocket.svg`} />
          </Button>
          <p> +1 energy</p>
        </div>
        <div>
          <Button disabled={launched} onClick={() => handleButtonClick("iron")}>
            <img src={`/img/iron-rocket.svg`} />
          </Button>
          <p> +1 iron</p>
        </div>
      </ContainerRocket>
    </>
  );
};

export default RocketManager;
