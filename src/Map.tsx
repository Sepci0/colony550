import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { TileWidth } from "./MapTile";

const Container = styled.div`
  position: relative;
  width: ${TileWidth}px * 50;
  height: ${TileWidth}px * 50;

  // Set the initial position of the camera
  position: absolute;
  top: 0;
  left: 0;

  // Only show a 10x10 grid
  overflow: hidden;
  width: ${TileWidth}px * 10;
  height: ${TileWidth}px * 10;
`;

const Tile = styled.div`
  position: absolute;
  width: ${TileWidth}px;
  height: ${TileWidth}px;

  // Create a grid of lines using a background image
  background-image: linear-gradient(to bottom, rgba(0, 255, 255, 0.55) 1px),
    linear-gradient(to right, rgba(255, 255, 0, 0.55) 1px);
  background-size: 50px 50px;
`;

const Map = () => {
  // Use React state to track the camera position
  const [cameraX, setCameraX] = useState(0);
  const [cameraY, setCameraY] = useState(0);

  // Update the position of the camera when the user moves it
  const handleCameraMove = (dx: number, dy: number) => {
    setCameraX(cameraX + dx);
    setCameraY(cameraY + dy);
  };

  // Listen for arrow key events and move the camera
  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      switch (event.key) {
        case "ArrowUp":
          handleCameraMove(0, -1);
          break;
        case "ArrowDown":
          handleCameraMove(0, 1);
          break;
        case "ArrowLeft":
          handleCameraMove(-1, 0);
          break;
        case "ArrowRight":
          handleCameraMove(1, 0);
          break;
      }
    };

    window.addEventListener("keydown", handleKeyDown);

    return () => {
      window.removeEventListener("keydown", handleKeyDown);
    };
  }, [cameraX, cameraY]);

  return (
    <Container style={{ top: -cameraY * 50, left: -cameraX * 50 }}>
      {/* Render 50x50 tiles here */}
      {Array.from({ length: 50 }, (_, y) =>
        Array.from({ length: 50 }, (_, x) => (
          <Tile key={`${x},${y}`} style={{ top: y * 50, left: x * 50 }} />
        ))
      )}
    </Container>
  );
};

export default Map;
