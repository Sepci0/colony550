import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const MenuContainer = styled.div`
  > a {
    color: white;
    text-transform: uppercase;
    display: block;
    border: solid 1px white;
    padding: 10px;
    border-radius: 10px;
    margin: 20px;
  }
`;

export default function MainMenu() {
  return (
    <MenuContainer>
      <h1>Colony 550</h1>
      <Link to={"/game"}> play game</Link>
      <Link to={"/credits"}> Credits</Link>
      <h3>How to play:</h3>
      <p>
        You are in command of the Mars colony.
        <br />
        Every 5 seconds a new colonist appear (left side bottom)
        <br />
        <br />
        Colonist needs water. To get it, build water siloses.
        <br />
        To build them, you need iron refineries. Its the gray building <br />
        But every building cost power to run. Build solar panels to get energy
        <br />
        <br />
        You have limited amount of buildings, <br />
        but you can get more by ordering rockets help from earth
        <br />
        (right bottom side)
        <br />
        <br />
        Your goal is to get as much colonist as passible
        <br /> Share the record on gamejam discord!
      </p>
    </MenuContainer>
  );
}
