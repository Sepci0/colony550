import React from "react";
import { Link } from "react-router-dom";
import { MenuContainer } from "./Credits";

export default function Lost() {
  return (
    <MenuContainer>
      <h3>You lose!</h3>
      <p>
        Twoja kolonia zdecydowała o wyborze innego zarządcy.
        <br />
        Spróbuj ponownie!
        <br />
        <br />
        <i style={{ fontSize: "0.8em" }}>
          TIP: budynki z czerwonym znakiem nie mają personelu.
          <br /> możesz obsadzać personel klikając na nie
        </i>
      </p>
      <a href={"/game"}>Restart</a>
      <Link to={"/"}>Main Menu</Link>
    </MenuContainer>
  );
}
