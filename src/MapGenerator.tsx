import React from "react";
import styled from "styled-components";
import MapTile, { TileWidth } from "./MapTile";
import Prompt from "./Pormpt";

const mapWidth = 40;

const MapContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  /* it just works */
  max-width: ${TileWidth * (mapWidth + 1) + TileWidth / 2}px;
  background-image: url("/img/bg.png");
`;

const MapGenerator = () => {
  const tiles = [];

  for (let i = 0; i < mapWidth; i++) {
    for (let j = 0; j < mapWidth; j++) {
      tiles.push(<MapTile key={`${i},${j}`} tileId={`${i},${j}`} />);
    }
  }

  return (
    <>
      <MapContainer>{tiles}</MapContainer>;
      <Prompt />
    </>
  );
};

export default MapGenerator;
